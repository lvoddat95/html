(function($){
    "use strict"; // Start of use strict  

     document.addEventListener(
                "DOMContentLoaded", () => {
                    new Mmenu( "#my-menu", {
                    extensions  : [ 'effect-slide-menu', 'pageshadow' ],
                    
                    counters    : true,
                    navbar      : {
                        title       : 'Menu'
                    },
                    navbars     : [
                         {
                            position    : 'top',
                            content     : [
                                'prev',
                                'title',
                                'close'
                            ]
                        }
                    ]
                });
                }
            );

    $(window).resize(function(){
        _owl_slider();
    });

    $(window).scroll(function(){
        _scroll_sticky();
    });

    $(document).ready(function(){
        _owl_slider();
        _slick_slider();

        if ( $('.cart-col-right').length > 0 ) {
            $('.cart-col-right').theiaStickySidebar({
                additionalMarginTop: 30
            });
        }

        // aside-box 
        $('.sidebar-overlay, .close-sidebar').on('click',function(event){
            $('body').removeClass('sidebar-aside');
            $('.sidebar-overlay').removeClass('toggle');
        });

        $('.toggle-btn').on('click',function(event){
            event.preventDefault();
            event.stopPropagation();
            $('body').toggleClass('sidebar-aside');
            $(this).parents('.sidebar-aside').find('.sidebar-overlay').toggleClass('toggle');
        });


    

            // var $nav_left = $('.nav-left > .navbar > li');
            // var $nav_right = $('.nav-right > .navbar > li');
            // var $nav_categories = $('.menu-category > .dropdown-menu > .navbar > li');

            // $nav_categories.clone().appendTo("#my-menu > ul");
            // $nav_left.clone().appendTo("#my-menu > ul");
            // $nav_right.clone().appendTo("#my-menu > ul");

            // $(".mb-tab-link").click(function (e) {
            //     e.preventDefault();
            //     $(".mb-tab-link").removeClass("active");
            //     $(this).addClass("active");
            //     $(".mb-content").hide();
            //     $($(this).attr("href")).show();
            // });
            
     


       

        $('.mobile-nav .navbar li:has(ul)').append('<span class="icon-sub-menu"></span>');
        $('.mobile-nav .mega-box:has(ul) .mega-box-title').append('<span class="icon-sub-menu"></span>');
        $(".icon-sub-menu").on("click",function(e){
            e.preventDefault();
            if ($(this).parent().hasClass('nav-item')) {
                $(this).parent().toggleClass('open');
            }else{
                $(this).parent().parent().toggleClass('open');
            }
        });

        $('.sidebar-toggle').on("click",function(e){
            e.preventDefault();
            $('body').append('<div class="overlay"></div>').toggleClass('show-menu');
        });

        $('body').on("click",".overlay",function(e){
            $('body').removeClass('show-menu');
            $(this).remove();
        });



        $("#toTop").click(function (e) {
            e.preventDefault();
           $("html, body").animate({scrollTop: 0}, 1000);
        });

        $('.slider-range').each(function(){
            var v_text = $(this).data('text');
            var v_min = parseInt($(this).data('min'));
            var v_max = parseInt($(this).data('max'));
            var v_step = parseInt($(this).data('step'));
            if (!v_text) { v_text = 'VND'};
            if (!v_min) { v_min = 0};
            if (!v_max) { v_max = 99999999};

            $(this).slider({
                range: true,
                orientation: "horizontal",
                min: v_min,
                max: v_max,
                values: [v_min, v_max],
                step: v_step,
                slide: function (event, ui) {
                    if (ui.values[0] == ui.values[1]) return false;
                    $(this).parent().find(".min_price").val(ui.values[0] + ' ' + v_text);
                    $(this).parent().find(".max_price").val(ui.values[1] + ' ' + v_text);
                }
            });
        });

        $( ".attribute-filter > a" ).click(function() {
            $(this).toggleClass('active');
        });


        $('.attribute-filter').each(function(){
            var LiN = $(this).find('a').length;
            if( LiN > 3){    
                $('a', this).eq(2).nextAll().hide().addClass('toggleable');
                $(this).append('<a class="more">Hiển thị thêm...</a>');    
            }
        });

        $('.attribute-filter').on('click','.more', function(){
            if( $(this).hasClass('less') ){    
                $(this).text('Hiển thị thêm...').removeClass('less');    
            }else{
                $(this).text('Thu gọn...').addClass('less'); 
            }
            $(this).siblings('a.toggleable').slideToggle();
        }); 

        if ($().fancybox) {
            $('[data-fancybox="images"]').fancybox({
                thumbs : {
                    autoStart : true
                }
            })
        }

        if ( $('.MagicZoom').length > 0 ) {
            MagicZoom.options = {
                'disable-zoom' : false,
                'selectors-change' : 'click',
                'zoom-width':400,
                'zoom-height':400
            }
        }

        $('.galery-image').on('click','.item:not(.more)', function(e){
            e.preventDefault();
            $('.galery-image .item.active').removeClass('active');
            $(this).addClass('active');
            var v_href = $(this).attr('href');
            $(this).closest('.product-image').find('.img-large > a').attr('href',v_href);
            $(this).closest('.product-image').find('.img-large > a > img').attr('src',v_href);
            $(this).closest('body').find('.MagicZoomBigImageCont img').attr('src',v_href);
        }); 

        $(document).on("click", ".desc-more", function(){ 
            $(this).toggleClass('collapsed');
            $(this).closest('.tab-s-content').children().eq(0).toggleClass('collapsed');
        }); 

        $(document).on("click", ".btn-comment", function(){ 
            $(this).closest('#customer-review').find('.comment-form-input').show();
        }); 


        $( ".close-comment" ).click(function() {
            $(this).parent().hide();
        });

         $("#buy-type").on("click", ".login-buy", function(){ 
            $(this).closest('#customer-review').find('.comment-form-input').show();
        }); 

    });

   

    function _scroll_sticky(){
        var $window       = $(window);
        var lastScrollTop = 0;
        var $header       = $('.header-top');
        var headerHeight  = $header.outerHeight();
        var windowTop  = $window.scrollTop();
    
        if ( windowTop >= headerHeight ) {
            $header.addClass( 'sticky' );
        } else {
            $header.removeClass( 'sticky' ).removeClass( 'show' );
        }

        if ( $header.hasClass( 'sticky' ) ) {
            if ( windowTop > lastScrollTop ) {
                $header.addClass( 'show' );
            } else {
                $header.removeClass( 'show' );
            }
        }
  
        lastScrollTop = windowTop;
    }


    //Carousel Slider Config
    function _owl_slider(){
        if($('.owl-slider').length>0){
            $('.owl-slider').each(function(){
                var seff = $(this);
                var item = seff.attr('data-item');
                var speed = seff.attr('data-speed');
                var itemres = seff.attr('data-itemres');
                var nav = seff.attr('data-navigation');
                var pag = seff.attr('data-pagination');
                var text_prev = seff.attr('data-prev');
                var text_next = seff.attr('data-next');
                var margin = seff.attr('data-margin');
                var stage_padding = seff.attr('data-stage_padding');
                var start_position = seff.attr('data-start_position');
                var merge = seff.attr('data-merge');
                var loop = seff.attr('data-loop');
                var mousewheel = seff.attr('data-mousewheel');
                var mousedrag = seff.attr('data-mousedrag');
                var pagination = false, navigation= false;
                var autoplay;
                var autoplaytimeout = 5000;
                if(!margin) margin = 0;
                if(!stage_padding) stage_padding = 0;
                if(!start_position) start_position = 0;
                if(!merge) merge = false; else merge = true;
                if(!loop) loop = false; else loop = true;
                if(!mousewheel) mousewheel = false; else mousewheel = true;
                if(!mousedrag) mousedrag = false; else mousedrag = true;
                if(speed != ''){
                    autoplay = true;
                    autoplaytimeout = parseInt(speed, 10);
                } else {
                    autoplay = false; 

                }
                
                // Navigation
                if(!nav) navigation = true;
                if(!pag) pagination = true;

                var dotsContainer = '';
                if (pag == 'pagi-container') dotsContainer = '.dotsContainer';
                var prev_text = '<i class="far fa-angle-left" aria-hidden="true"></i>';
                var next_text = '<i class="far fa-angle-right" aria-hidden="true"></i>';
                if(text_prev) prev_text = text_prev;
                if(text_next) next_text = text_next;
                if(itemres == '' || itemres === undefined){
                    if(item == '1') itemres = '0:1,480:1,768:1,1200:1';
                    if(item == '2') itemres = '0:1,480:1,768:2,1200:2';
                    if(item == '3') itemres = '0:1,480:2,768:2,992:3';
                    if(item == '4') itemres = '0:1,480:2,840:3,1200:4';
                    if(item >= '5') itemres = '0:1,480:2,768:3,1024:4,1200:'+item;
                }
                itemres = itemres.split(',');
                var responsive = {};
                var i;
                for (i = 0; i < itemres.length; i++) { 
                    itemres[i] = itemres[i].split(':');
                    var res_dv = {};
                    res_dv.items = parseInt(itemres[i][1], 10);
                    responsive[itemres[i][0]] = res_dv;
                }

                var destroy = seff.attr('data-destroy');

                var owlCarousel = function(){
                    seff.owlCarousel({
                        items: parseInt(item, 10),
                        margin: parseInt(margin, 10),
                        loop: loop,
                        stagePadding: parseInt(stage_padding, 10),
                        startPosition: parseInt(start_position, 10),
                        nav:navigation,
                        navText: [prev_text,next_text],
                        responsive: responsive,
                        autoplay: autoplay,
                        autoplayTimeout: autoplaytimeout,
                        dots: pagination,
                        onTranslate: _owl_on_translate,
                        onTranslated: _owl_on_translated,
                        onInitialize: _owl_on_initialize,
                        dotsContainer: dotsContainer,
                        mouseDrag:mousedrag,
                        rewind: true,
                        smartSpeed: 500,
                    });
                }

                if (destroy) {
                    if ( $(window).width() > 768 ) {
                        owlCarousel();
                    } else {
                        seff.trigger('destroy.owl.carousel').show();
                    }
                }else{
                    owlCarousel()
                }

                

                if(mousewheel){
                    seff.on('mousewheel', '.owl-stage', function (e) {
                        if (e.deltaY>0) {
                            seff.trigger('next.owl');
                        } else {
                            seff.trigger('prev.owl');
                        }
                        e.preventDefault();
                    });
                }
            });         
        }
    }

    function _owl_on_translate(event){ }

    function _owl_on_translated(event){
        var element   = event.target;
        $(element).find('.owl-item').each(function(){
            var check = $(this).hasClass('active');
            if(check==true){
                $(this).find('.animated').each(function(){
                    var anime = $(this).attr('data-animated');
                    $(this).addClass(anime);
                });
            }else{
                $(this).find('.animated').each(function(){
                    var anime = $(this).attr('data-animated');
                    $(this).removeClass(anime);
                });
            }
        })
    }

    function _owl_on_initialize(){
        if($('.bg-slider').length<0) return;
        $('.bg-slider .item-slider').each(function(){
            $(this).find('.slider-img a img').css('height',$(this).find('.slider-img a img').attr('height'));
            var src=$(this).find('.slider-img a img').attr('src');
            $(this).find('.slider-img').css('background-image','url("'+src+'")');
        }); 
    }
    function _slick_slider(){
        if($('.slider').length>0){

            $(".slider").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                asNavFor: ".slider-nav-thumbnails",
                prevArrow: null,
                nextArrow: null,
            });

            $(".slider-nav-thumbnails").slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: ".slider",
                dots: false,
                focusOnSelect: true,
                prevArrow: '<i class="fal fa-angle-left slick-arrow-left"></i>',
                nextArrow: '<i class="fal fa-angle-right slick-arrow-right"></i>',

            });

            // Remove active class from all thumbnail slides
            $(".slider-nav-thumbnails .slick-slide").removeClass("slick-active");

            // Set active class to first thumbnail slides
            $(".slider-nav-thumbnails .slick-slide").eq(0).addClass("slick-active");

            // On before slide change match active thumbnail to current slide
            $(".slider").on("beforeChange", function (event, slick, currentSlide, nextSlide) {
                var mySlideNumber = nextSlide;
                $(".slider-nav-thumbnails .slick-slide").removeClass("slick-active");
                $(".slider-nav-thumbnails .slick-slide").eq(mySlideNumber).addClass("slick-active");
            });
        }

    }

})(jQuery);